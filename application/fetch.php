<?php
define('HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'nim_finder');

$conn = mysqli_connect(HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME) or die("error");
$output = "";
$faculty = "";
$year = "";

if (isset($_POST["fakultas"])){
    $faculty = mysqli_real_escape_string($conn, $_POST["fakultas"]);
}

if (isset($_POST["angkatan"])){
    $year = mysqli_real_escape_string($conn, $_POST["angkatan"]);
    $year = substr($year, 2, 2);
}

if(isset($_POST["query"])){
    $search = mysqli_real_escape_string($conn, $_POST["query"]);

    if (strlen($search) > 0){
        $query = "SELECT * FROM mahasiswa WHERE nama LIKE '%".$search."%'
        OR nim_tpb LIKE '%".$search."%' 
        OR nim_jurusan LIKE '%".$search."%'
        OR jurusan LIKE '%".$search."%' 
        ORDER BY nama
        LIMIT 300";

        if (strlen($faculty) > 0){
            if (strlen($year) > 0){
                $query = "SELECT * FROM mahasiswa WHERE fakultas LIKE  '%".$faculty."%' AND  nim_jurusan LIKE '___".$year."___'  AND (nama LIKE '%".$search."%'
                OR nim_tpb LIKE '%".$search."%' OR nim_jurusan LIKE '%".$search."%'
                OR jurusan LIKE '%".$search."%') 
                ORDER BY nama
                LIMIT 300"; 
            }else{
                $query = "SELECT * FROM mahasiswa WHERE fakultas LIKE  '%".$faculty."%' AND (nama LIKE '%".$search."%'
                OR nim_tpb LIKE '%".$search."%' OR nim_jurusan LIKE '%".$search."%'
                OR jurusan LIKE '%".$search."%') 
                ORDER BY nama
                LIMIT 300";
            }
        }
        
        $result = mysqli_query($conn, $query);
        if(mysqli_num_rows($result) > 0){
            $output .= '
            <div class="table-responsive" style="overflow-x:auto">
            <table class="table table-striped">
            <thead>
                <tr>
                <th>Nama</th>
                <th>NIM TPB</th>
                <th>NIM Jurusan</th>
                <th>Jurusan</th>
                </tr>
            </thead>
            <tbody>';
            while($row = mysqli_fetch_array($result)){
                $output .= '
                <tr>
                    <td>'.$row["nama"].'</td>
                    <td>'.$row["nim_tpb"].'</td>
                    <td>'.$row["nim_jurusan"].'</td>
                    <td>'.$row["jurusan"].'</td>
                </tr>
                ';
            }
            $output .= '</tbody> </table>';
            echo $output;
        }else{
            echo '
                <div class="query-not-found">
                    <h3> Mahasiswa tidak ditemukan :( </h3>
                </div>';
        }
    }
}