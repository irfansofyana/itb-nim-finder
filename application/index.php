<html>
    <head>
        <meta http-equiv="Content Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <title>NIM Finder ITB</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" /> 
        <link href="index.css" rel="stylesheet" type="text/css">
        <link href="public/I-logo.png" rel="icon" type="image/png" size="32x32">
    </head>
    <body>
        <div class="header">
            <a href="index.php" class="nounderline">
                <strong>NIM FINDER ITB </strong> by irfansofyana
            </a>
        </div>
        <div class="form-group">
            <div class="input-group">
                <input type="text" name="search_text" id="search_text" 
                      placeholder="Cari Nama / NIM / Jurusan ..." class="form-control" />
                <span class="input-group-addon">
                    <button id="myBtn">
                        Filter
                    </button>
                </span>
            </div>
        </div>

        <!-- The Modal -->
        <div id="myModal" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <span class="close">&times;</span>
                    <label for="fakultas"> Fakultas </label>
                    <select id = "fakultas" class="form-control form-control-lg" name="fakultas">
                        <option value="" selected disabled hidden>Pilih Fakultas</option>
                        <option value="FSRD">FSRD</option>
                        <option value="FMIPA">FMIPA</option>
                        <option value="SITH-S">SITH-S</option>
                        <option value="SF">SF</option>
                        <option value="FITB">FITB</option>
                        <option value="FTTM">FTTM</option>
                        <option value="STEI">STEI</option>
                        <option value="FTSL">FTSL</option>
                        <option value="FTI">FTI</option>
                        <option value="FTMD">FTMD</option>
                        <option value="SITH-R">SITH-R</option>
                        <option value="SAPPK">SAPPK</option>
                        <option value="SBM">SBM</option>
                        <option value="FTI-J">FTI-J</option>
                        <option value="FTSL-J">FTSL-J</option>
                    </select>
                    <label for="angkatan"> Angkatan </label>
                    <select id ="angkatan" class="form-control form-control-lg" name="angkatan">
                        <option value="" selected disabled hidden>Pilih Angkatan</option>
                        <option value="2013">2013</option>
                        <option value="2014">2014</option>
                        <option value="2015">2015</option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                    </select>
            </div>
        </div>
        <div id="result"></div>
    </body>
</html>

<script>
    $(document).ready(function(){
        load_data();
        
        function load_data(query){
            var faculty, year;
            faculty = $("#fakultas").val();
            year = $("#angkatan").val();


            $.ajax({
                url:"fetch.php",
                method:"POST",
                data:{
                    query:query,
                    fakultas:faculty,
                    angkatan:year
                },
                success:function(data){
                    $('#result').html(data);
                }
            });
        }
        
        $('#search_text').keyup(function(){
            var search = $(this).val();
            if(search != ''){
                load_data(search);
            }
            else{
                load_data();
            }
        });
    });
        
    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    btn.onclick = function() {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>